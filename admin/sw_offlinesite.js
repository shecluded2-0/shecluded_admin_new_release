var offlineCacheName = 'Shecluded Admin';
var offlineURL = "offline.html";

var offlineSiteCache = 'offlineSite';



var resourcesToCache = [
	'../assets/vendors/css/vendors.min.css',
    '../assets/vendors/css/forms/toggle/switchery.min.css',
    '../assets/vendors/css/pickers/daterange/daterangepicker.css',
    '../assets/vendors/css/pickers/datetime/bootstrap-datetimepicker.css',
    '../assets/vendors/css/pickers/pickadate/pickadate.css',
    '../assets/vendors/css/tables/datatable/datatables.min.css',
    '../assets/vendors/css/extensions/shepherd.min.css',
    '../assets/css/bootstrap.min.css',
    '../assets/css/bootstrap-extended.min.css',
    '../assets/css/colors.min.css',
    '../assets/css/components.min.css',
    '../assets/css/core/menu/menu_types/vertical-menu-modern.css',
    '../assets/css/core/colors/palette-gradient.min.css',
    '../assets/css/plugins/pickers/daterange/daterange.min.css',
    '../assets/css/pages/page-users.min.css',
    '../assets/css/plugins/forms/switch.min.css',
    '../assets/css/custom.css',
     '../assets/css/placeholder.css',
     '../assets/vendors/js/vendors.min.js',
    '../assets/vendors/js/pickers/dateTime/moment-with-locales.min.js',
    '../assets/vendors/js/pickers/dateTime/bootstrap-datetimepicker.min.js',
    '../assets/vendors/js/pickers/pickadate/picker.js',
    '../assets/vendors/js/pickers/pickadate/picker.date.js',
    '../assets/vendors/js/pickers/pickadate/picker.time.js',
    '../assets/vendors/js/pickers/pickadate/legacy.js',
    '../assets/vendors/js/pickers/daterange/daterangepicker.js',
    '../assets/vendors/js/scripts/pickers/dateTime/bootstrap-datetime.min.js',
    '../assets/vendors/js/scripts/pickers/dateTime/pick-a-datetime.min.js',
    '../assets/vendors/js/tables/datatable/datatables.min.js',
    '../assets/vendors/js/tables/datatable/dataTables.buttons.min.js',
    '../assets/vendors/js/tables/datatable/buttons.flash.min.js',
    '../assets/vendors/js/tables/datatable/jszip.min.js',
    '../assets/vendors/js/tables/datatable/pdfmake.min.js',
    '../assets/vendors/js/tables/datatable/vfs_fonts.js',
    '../assets/vendors/js/tables/datatable/buttons.html5.min.js',
    '../assets/vendors/js/tables/datatable/buttons.print.min.js',
    '../assets/js/core/app-menu.min.js',
    '../assets/js/core/app.min.js',
    '../assets/js/scripts/customizer.min.js',
    '../assets/js/scripts/forms/switch.min.js',
    '../assets/js/scripts/pages/bootstrap-toast.min.js',
	'admin/admins.php',
	'admin/clients.php',
	'admin/header.php',
	'admin/footer.php',
	'admin/communities.php',
	'admin/course_topic.php',
	'admin/course-category.php',
	'admin/courses.php',
	'admin/dashboard.php',
	'admin/edit_userinfo.php',
	'admin/failed_trans.php',
	'admin/getPlanPackage.php',
	'admin/insurance-package.php',
	'admin/insurance-plan.php',
	'admin/lessons.php',
	'admin/loadCategory.php',
	'admin/loadCourse.php',
	'admin/loadInsurancePlans.php',
	'admin/loadTopic.php',
	'admin/loans_purpose.php',
	'admin/loans-trans.php',
	'admin/ref_query.php',
	'admin/savings.php',
	'admin/side-menus.php',

];

this.addEventListener('install', function(event){

	console.log('Service worker installing');

	event.waitUntil(

		caches.open(offlineSiteCache)
		.then(function(cache){
			//return cache.addAll(resourcesToCache);
            return cache.add(offlineURL);
		})
		.then(function(){
			return self.skipWaiting();
		})

	);

});

this.addEventListener('fetch', function(event){

	event.respondWith(

		fetch(event.request)
		.then(function(response){
			var responseClone = response.clone();
			caches.open(offlineCacheName)
			.then(function(cache){
				cache.put(event.request, responseClone);
			})
			return response;
		})
		.catch(function(){
			return caches.match(event.request)
			.then(function(response){
				return response || caches.match(offlineURL);
			});
		})

	);

});

/*this.addEventListener('fetch', function(event){

	event.respondWith(

		fetch(event.request)
		.catch(function(){
			return caches.match(event.request);
		})

	);

});*/