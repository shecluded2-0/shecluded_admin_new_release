<?php
    include("header.php");
    include("top-nav.php");
    include("side-menus.php");
?>
  
<style>

.dashboard{
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100vh;

}

</style>

  

    <!-- BEGIN: Content-->
    <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0"></h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-12">
               
              </div>
            </div>
          </div>
          <div class="content-header-right col-md-6 col-12 mb-md-0 mb-2">
            
          </div>
        </div>
        
        <div class="content-body">
            <!-- tour guide start -->
            <section class=" tour-wrapper">
                <!-- Basic Tour Start here -->
                <div class="basic-tour">
                    <div class="row">
                        <div class="col-sm-12 dashboard">
                        <h3 class="content-header-title mb-0">Dashboard</h3>
                        </div>
                    </div>
                </div>
            </section>
             <!-- tour guide ends -->
        </div>
        
      </div>
    </div>
    <!-- END: Content-->



<?php
    include("customizer.php");
    include("footer.php");
?>