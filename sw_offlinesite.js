var offlineCacheName = 'Shecluded';
var offlineURL = "offline.html";

var offlineSiteCache = 'offlineSite';



var resourcesToCache = [
    'assets/vendors/css/vendors.min.css',
    'assets/vendors/css/forms/icheck/icheck.css',
    'assets/vendors/css/forms/icheck/custom.css',
    'assets/css/bootstrap.min.css',
    'assets/css/bootstrap-extended.min.css',
    'assets/css/colors.min.css',
    'assets/css/components.min.css',
    'assets/css/core/menu/menu_types/vertical-menu-modern.css',
    'assets/css/core/colors/palette-gradient.min.css',
    'assets/css/pages/login-register.min.css',
     'assets/vendors/js/vendors.min.js',
    'assets/vendors/js/forms/icheck/icheck.min.js',
    'assets/vendors/js/forms/validation/jqBootstrapValidation.js',
    'assets/js/core/app-menu.min.js',
    'assets/js/core/app.min.js',
    'assets/js/scripts/pages/bootstrap-toast.min.js',
    'assets/js/login.js',
    'auth.php'
    

];

this.addEventListener('install', function(event){

	console.log('Service worker installing');

	event.waitUntil(

		caches.open(offlineSiteCache)
		.then(function(cache){
			//return cache.addAll(resourcesToCache);
            return cache.add(offlineURL);
		})
		.then(function(){
			return self.skipWaiting();
		})

	);

});

this.addEventListener('fetch', function(event){

	event.respondWith(

		fetch(event.request)
		.then(function(response){
			var responseClone = response.clone();
			caches.open(offlineCacheName)
			.then(function(cache){
				cache.put(event.request, responseClone);
			})
			return response;
		})
		.catch(function(){
			return caches.match(event.request)
			.then(function(response){
				return response || caches.match(offlineURL);
			});
		})

	);

});

/*this.addEventListener('fetch', function(event){

	event.respondWith(

		fetch(event.request)
		.catch(function(){
			return caches.match(event.request);
		})

	);

});*/